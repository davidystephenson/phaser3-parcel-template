import Phaser from 'phaser'

import Position from './Position'

export default class Edge {
  a: Position
  b: Position

  constructor ({ a, b }) {
    this.a = a

    this.b = b
  }

  align (): Phaser.Geom.Line {
    return new Phaser.Geom.Line(
      this.a.x, this.a.y, this.b.x, this.b.y
    )
  }
}
