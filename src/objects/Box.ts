import Position from './Position'
import Size from './Size'
import Edge from './Edge'

export default class Box {
  topLeft: Position
  topRight: Position
  bottomLeft: Position
  bottomRight: Position
  size: Size
  edges: Edge[]

  constructor ({
    topLeft, size
  }) {
    this.topLeft = topLeft
    this.size = size

    const right = this.topLeft.x + this.size.width

    this.topRight = new Position({ ...topLeft, x: right })

    this.edges = []
    const edge = new Edge({
      a: this.topLeft, b: this.topRight
    })
    this.edges.push(edge)
  }
}
