module.exports = {
  root: true,
  parserOptions: {
    ecmaVersion: 6,
    sourceType: 'module',
    project: './tsconfig.json'
  },
  env: {
    es6: true,
    browser: true
  },
  extends: [
    'standard-with-typescript'
  ],
  rules: {}
}
